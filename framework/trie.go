package framework

import (
	"fmt"
	"strings"
)

// Tree 树
type Tree struct {
	// root 根节点
	root *node
}

// node 节点
type node struct {
	// isLast 该节点是否能成为一个独立的uri，是否自身就是一个终极节点
	isLast bool
	// segment uri中的字符串
	segment string
	// handler 控制器
	handler ControllerHandler
	// childs 子节点
	childs []*node
}

func newNode() *node {
	return &node{
		isLast:  false,
		segment: "",
		handler: nil,
		childs:  []*node{},
	}
}

func NewTree() *Tree {
	return &Tree{
		root: newNode(),
	}
}

// isWildSegment 判断一个segment是否是通配segment，即以:开头
func isWildSegment(segment string) bool {
	return strings.HasPrefix(segment, ":")
}

// filterChildNodes 过滤下一层满足segment规则的子节点
func (n *node) filterChildNodes(segment string) []*node {
	if len(n.childs) == 0 {
		return nil
	}

	// 如果segment是通配符，则所有下一层子节点都满足需求
	if isWildSegment(segment) {
		return n.childs
	}

	nodes := make([]*node, 0, len(n.childs))
	// 过滤所有的下一层子节点
	for _, cnode := range n.childs {
		if isWildSegment(cnode.segment) {
			// 如果下一层有通配符，则满足需求
			nodes = append(nodes, cnode)
		} else if cnode.segment == segment {
			// 如果下一层子节点没有通配符，但是文本完全匹配，则满足需求
			nodes = append(nodes, cnode)
		}
	}
	return nodes
}

// matchNode 判断路由是否已经在节点的所有子节点树中存在了
func (n *node) matchNode(uri string) *node {
	// 分隔uri
	segments := strings.SplitN(uri, "/", 2)
	// 第一部分用于匹配下一层子节点
	segment := segments[0]
	if !isWildSegment(segment) {
		segment = strings.ToUpper(segment)
	}
	// 匹配符合的下一层子节点
	cnodes := n.filterChildNodes(segment)
	// 如果没有符合的子节点，那么说明uri之前不存在，直接返回 nil
	if len(cnodes) == 0 {
		return nil
	}

	// 如果只有一个segment，则是最后一个标记
	if len(segments) == 1 {
		// 如果已经是最后一个节点，判断这些cnode是否有isLast标志
		for _, tn := range cnodes {
			if tn.isLast {
				return tn
			}
		}
		// 都不是最后一个节点
		return nil
	}

	// 如果有多个segment，递归每个子节点继续查找
	for _, tn := range cnodes {
		tnMath := tn.matchNode(segments[1])
		if tnMath != nil {
			return tnMath
		}
	}
	return nil
}

// AddRouter 增加路由节点, 路由节点有先后顺序
//
// /book/list
//
// /book/:id (冲突)
//
// /book/:id/name
//
// /book/:student/age
//
// /:user/name
//
// /:user/name/:age (冲突)
func (tree *Tree) AddRouter(uri string, handler ControllerHandler) error {
	n := tree.root
	if n.matchNode(uri) != nil {
		return fmt.Errorf("route %q exists", uri)
	}

	segments := strings.Split(uri, "/")
	for index, segment := range segments {
		// 最终进入 Node segment 的字段
		if !isWildSegment(segment) {
			segment = strings.ToUpper(segment)
		}
		isLast := index == len(segments)-1

		var objNode *node // 标记是否有合适的子节点

		childNodes := n.filterChildNodes(segment)
		// 如果有匹配的子节点
		if len(childNodes) > 0 {
			// 如果有segment相同的子节点，则选择这个子节点
			for _, cnode := range childNodes {
				if cnode.segment == segment {
					objNode = cnode
					break
				}
			}
		}

		if objNode == nil {
			// 创建一个当前node的节点
			cnode := newNode()
			cnode.segment = segment
			if isLast {
				cnode.isLast = true
				cnode.handler = handler
			}
			n.childs = append(n.childs, cnode)
			objNode = cnode
		}

		n = objNode
	}
	return nil
}

func (tree *Tree) FindHandler(uri string) ControllerHandler {
	matchNode := tree.root.matchNode(uri)
	if matchNode == nil {
		return nil
	}
	return matchNode.handler
}
