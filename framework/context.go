package framework

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"
	"sync"
	"time"
)

type Context struct {
	request       *http.Request
	responseWrite http.ResponseWriter
	ctx           context.Context
	handler       ControllerHandler

	// 是否超时标记位
	hasTimeout bool
	// 写保护机制
	writeMux *sync.Mutex
}

func NewContext(r *http.Request, w http.ResponseWriter) *Context {
	return &Context{
		request:       r,
		responseWrite: w,
		ctx:           r.Context(),
		writeMux:      &sync.Mutex{},
	}
}

// #region base function

func (c *Context) WriteMux() *sync.Mutex {
	return c.writeMux
}
func (c *Context) GetRequest() *http.Request {
	return c.request
}
func (c *Context) GetResponse() http.ResponseWriter {
	return c.responseWrite
}
func (c *Context) SetHasTimeout() {
	c.hasTimeout = true
}
func (c *Context) HasTimeout() bool {
	return c.hasTimeout
}

// #endregion

func (c *Context) BaseContext() context.Context {
	return c.ctx
}

// #region implement context.Context

func (c *Context) Deadline() (deadline time.Time, ok bool) {
	return c.BaseContext().Deadline()
}

func (c *Context) Done() <-chan struct{} {
	return c.BaseContext().Done()
}

func (c *Context) Err() error {
	return c.BaseContext().Err()
}
func (c *Context) Value(key any) any {
	return c.BaseContext().Value(key)
}

// #endregion

// #region query url
func (c *Context) QueryInt(key string, def int) int {
	params := c.QueryAll()
	if vals, ok := params[key]; ok {
		len := len(vals)
		if len > 0 {
			intval, err := strconv.Atoi(vals[len-1])
			if err != nil {
				return def
			}
			return intval
		}
	}
	return def
}
func (c *Context) QueryString(key string, def string) string {
	params := c.QueryAll()
	if vals, ok := params[key]; ok {
		len := len(vals)
		if len > 0 {
			return vals[len-1]
		}
	}
	return def
}
func (c *Context) QueryArray(key string, def []string) []string {
	parmas := c.QueryAll()
	if vals, ok := parmas[key]; ok {
		return vals
	}
	return def
}
func (c *Context) QueryAll() map[string][]string {
	if c.request != nil {
		return map[string][]string(c.request.URL.Query())
	}
	return map[string][]string{}
}

// #endregion

// #region form post
func (c *Context) FormInt(key string, def int) int {
	params := c.FormAll()
	if vals, ok := params[key]; ok {
		len := len(vals)
		if len > 0 {
			intval, err := strconv.Atoi(vals[len-1])
			if err != nil {
				return def
			}
			return intval
		}
	}
	return def
}
func (c *Context) FormString(key string, def string) string {
	params := c.FormAll()
	if vals, ok := params[key]; ok {
		len := len(vals)
		if len > 0 {
			return vals[len-1]
		}
	}
	return def
}
func (c *Context) FormArray(key string, def []string) []string {
	parmas := c.FormAll()
	if vals, ok := parmas[key]; ok {
		return vals
	}
	return def
}
func (c *Context) FormAll() map[string][]string {
	if c.request != nil {
		return map[string][]string(c.request.PostForm)
	}
	return map[string][]string{}
}

// #endregion

// #region application/json post
func (c *Context) BindJson(obj any) error {
	if c.request != nil {
		body, err := io.ReadAll(c.request.Body)
		if err != nil {
			return err
		}
		c.request.Body = io.NopCloser(bytes.NewBuffer(body))

		if err := json.Unmarshal(body, obj); err != nil {
			return err
		}
	} else {
		return errors.New("c.request empty")
	}
	return nil
}

// #endregion

// #region response
func (c *Context) Json(status int, obj any) error {
	if c.HasTimeout() {
		return nil
	}
	c.responseWrite.Header().Set("Content-Type", "application/json")
	c.responseWrite.WriteHeader(status)
	buf, err := json.Marshal(obj)
	if err != nil {
		c.responseWrite.WriteHeader(500)
		return err
	}
	c.responseWrite.Write(buf)
	return nil
}
func (c *Context) Html(status int, obj any, template string) error {
	return nil
}
func (c *Context) Text(status int, obj any) error {
	return nil
}

// #endregion
