package framework

import (
	"log"
	"net/http"
	"strings"
)

// Core 构架核心结构
type Core struct {
	router map[string]*Tree
}

// NewCore 初始化核心结构
func NewCore() *Core {
	router := map[string]*Tree{}
	router["GET"] = NewTree()
	router["POST"] = NewTree()
	router["PUT"] = NewTree()
	router["DELETE"] = NewTree()
	return &Core{
		router: router,
	}
}

func (c *Core) Get(url string, handler ControllerHandler) {
	if err := c.router["GET"].AddRouter(url, handler); err != nil {
		log.Fatal("add router error: ", err)
	}
}
func (c *Core) Post(url string, handler ControllerHandler) {
	if err := c.router["POST"].AddRouter(url, handler); err != nil {
		log.Fatal("add router error: ", err)
	}
}
func (c *Core) Put(url string, handler ControllerHandler) {
	if err := c.router["PUT"].AddRouter(url, handler); err != nil {
		log.Fatal("add router error: ", err)
	}
}
func (c *Core) Delete(url string, handler ControllerHandler) {
	if err := c.router["DELETE"].AddRouter(url, handler); err != nil {
		log.Fatal("add router error: ", err)
	}
}

func (c *Core) Group(prefix string) IGroup {
	return NewGroup(c, prefix)
}

// FindRouteByRequest 匹配路由
func (c *Core) FindRouteByRequest(r *http.Request) ControllerHandler {
	uri := r.URL.Path
	method := r.Method
	upperMethod := strings.ToUpper(method)

	// 查找第一层map
	if methodHandler, ok := c.router[upperMethod]; ok {
		return methodHandler.FindHandler(uri)
	}
	return nil
}

// ServeHTTP 核心框架实现 Handler 接口
func (c *Core) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	ctx := NewContext(r, w)

	router := c.FindRouteByRequest(r)
	if router == nil {
		ctx.Json(404, "not found")
		return
	}
	if err := router(ctx); err != nil {
		ctx.Json(500, "inner error")
		return
	}
}
