package framework

type IGroup interface {
	// HTTP Get
	Get(string, ControllerHandler)
	// HTTP Post
	Post(string, ControllerHandler)
	// HTTP Put
	Put(string, ControllerHandler)
	// HTTP Delete
	Delete(string, ControllerHandler)
	// 嵌套group
	Group(string) IGroup
}

type Group struct {
	core   *Core
	parent *Group // 指向上一个Group,  如果有的话
	prefix string
}

func NewGroup(c *Core, prefix string) *Group {
	return &Group{
		core:   c,
		parent: nil,
		prefix: prefix,
	}
}

func (g *Group) getAbsolutePrefix() string {
	if g.parent == nil {
		return g.prefix
	}
	return g.parent.getAbsolutePrefix() + g.prefix
}

// HTTP Get
func (g *Group) Get(uri string, handler ControllerHandler) {
	uri = g.getAbsolutePrefix() + uri
	g.core.Get(uri, handler)
}

// HTTP Post
func (g *Group) Post(uri string, handler ControllerHandler) {
	uri = g.getAbsolutePrefix() + uri
	g.core.Post(uri, handler)
}

// HTTP Put
func (g *Group) Put(uri string, handler ControllerHandler) {
	uri = g.getAbsolutePrefix() + uri
	g.core.Put(uri, handler)
}

// HTTP Delete
func (g *Group) Delete(uri string, handler ControllerHandler) {
	uri = g.getAbsolutePrefix() + uri
	g.core.Delete(uri, handler)
}

// 嵌套group
func (g *Group) Group(uri string) IGroup {
	cgroup := NewGroup(g.core, uri)
	cgroup.parent = g
	return cgroup
}
