package main

import (
	"net/http"

	"codeberg.org/lsp/hand-in-hand-go-web-framework/framework"
)

func main() {
	core := framework.NewCore()
	registerRouter(core)
	server := &http.Server{
		Handler: core,
		Addr:    ":12345",
	}
	server.ListenAndServe()
}
