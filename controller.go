package main

import (
	"context"
	"log"
	"time"

	"codeberg.org/lsp/hand-in-hand-go-web-framework/framework"
)

func FooCounterHandler(c *framework.Context) error {
	finish := make(chan struct{}, 1)
	panicChan := make(chan any, 1)

	durationCtx, cancel := context.WithTimeout(c.BaseContext(), time.Duration(1*time.Second))
	defer cancel()

	go func() {
		defer func() {
			if p := recover(); p != nil {
				panicChan <- p
			}
		}()

		time.Sleep(10 * time.Second)
		c.Json(200, "OK")
		finish <- struct{}{}
	}()

	select {
	case p := <-panicChan:
		c.WriteMux().Lock()
		defer c.WriteMux().Unlock()
		log.Println(p)
		c.Json(500, "panic")
	case <-finish:
		log.Println("finish")
	case <-durationCtx.Done():
		c.WriteMux().Lock()
		defer c.WriteMux().Unlock()
		c.Json(500, "time out")
		c.SetHasTimeout()
	}
	return nil
}
